package runner;

import org.junit.runner.RunWith;
import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(features="src/test/resources/features", glue="steps_definition", tags= {"@CasoExitoso, @CasoFallidoOrigenDestinoIgual"})
public class LoginRunner {

}
