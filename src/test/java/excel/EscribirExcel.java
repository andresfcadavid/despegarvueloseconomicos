package excel;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class EscribirExcel {

	public static void writeExcel(String[] vecDatos) throws IOException {

		// Create an object of File class to open xlsx file

		// File file = new
		// File("D:\\Users\\afcadavi\\Documents\\Vueloseconomicos.xlsx");

		// Create an object of FileInputStream class to read excel file

		// FileInputStream inputStream = new FileInputStream(file);

		XSSFWorkbook Libro = new XSSFWorkbook();

		// Read excel sheet by sheet name

		Sheet hoja = Libro.createSheet("Hoja1");

		Row primeraFila = hoja.createRow(0);
		Cell celda0 = primeraFila.createCell(0);
		celda0.setCellValue("Vuelos encontrados");

		// Create a loop over the cell of newly created Row

		for (int j = 1; j <= vecDatos.length; j++) {
			
			// Crear la fila
			Row nuevafila = hoja.createRow(j);

			Cell cell = nuevafila.createCell(0);

			cell.setCellValue(vecDatos[j - 1]);
			if (j == 1) {
				XSSFCellStyle style = Libro.createCellStyle();
                style.setFillForegroundColor(HSSFColor.GREEN.index);
                style.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);  
                           cell.setCellStyle(style); // se a�ade el style crea anteriormente 
                       

			}
		}

		// Close input stream

		// inputStream.close();

		// Create an object of FilearchivoSalida class to create write data in excel
		// file

		FileOutputStream archivoSalida = new FileOutputStream("D:\\Users\\afcadavi\\Documents\\Vueloseconomicos.xlsx");

		// write data in the excel file

		Libro.write(archivoSalida);

		// close output stream

		archivoSalida.close();

	}

}
