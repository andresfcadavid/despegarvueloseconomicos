package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;

public class DespegarHomePage {

	public void esperarSegundos(int segundos) {
		synchronized (driver) {
			try {
				driver.wait(segundos * 1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	private static WebDriver driver;
	// Creo el constructor de �ste misma clase

	public DespegarHomePage(WebDriver driver) {
		this.driver = driver;
	}

	public void cerrarAprovechaNuestrosBeneficios() {
		driver.findElement(By.xpath("/html/body/div[16]/div/div[1]/span")).click();
	}

	public void clicEnVuelos() {
		driver.findElement(By.xpath("//span[text() =\"Vuelos\"]")).click();

	}

	public void IngresarOrigen(String origen) {
		driver.findElement(By.xpath(
				"//input[@placeholder = \"Ingresa desde d�nde viajas\" and @class=\"input-tag sbox-main-focus sbox-bind-reference-flight-roundtrip-origin-input sbox-primary sbox-places-first places-inline\" ]"))
				.clear();
		driver.findElement(By.xpath(
				"//input[@placeholder = \"Ingresa desde d�nde viajas\" and @class=\"input-tag sbox-main-focus sbox-bind-reference-flight-roundtrip-origin-input sbox-primary sbox-places-first places-inline\" ]"))
				.sendKeys(origen);

		esperarSegundos(2);

		driver.findElement(By.xpath(
				"//input[@placeholder = \"Ingresa desde d�nde viajas\" and @class=\"input-tag sbox-main-focus sbox-bind-reference-flight-roundtrip-origin-input sbox-primary sbox-places-first places-inline\" ]"))
				.sendKeys(Keys.ENTER);

	}

	public void IngresarDestino(String destino) {

		driver.findElement(By.xpath(
				"//input[@placeholder= \"Ingresa hacia d�nde viajas\" and @class=\"input-tag sbox-main-focus sbox-bind-reference-flight-roundtrip-destination-input sbox-secondary sbox-places-second places-inline\"]"))
				.clear();

		driver.findElement(By.xpath(
				"//input[@placeholder= \"Ingresa hacia d�nde viajas\" and @class=\"input-tag sbox-main-focus sbox-bind-reference-flight-roundtrip-destination-input sbox-secondary sbox-places-second places-inline\"]"))
				.sendKeys(destino);

		esperarSegundos(2);

		driver.findElement(By.xpath(
				"//input[@placeholder= \"Ingresa hacia d�nde viajas\" and @class=\"input-tag sbox-main-focus sbox-bind-reference-flight-roundtrip-destination-input sbox-secondary sbox-places-second places-inline\"]")).sendKeys(Keys.ENTER);
			}
	
	public void IngresarFecha() {
		driver.findElement(By.xpath("//input [@placeholder=\"Partida\" and @class=\"input-tag sbox-bind-disable-start-date sbox-bind-value-start-date-segment-1 sbox-bind-reference-flight-start-date-input -sbox-3-no-radius-right\"]" )).click();
		
		driver.findElement(By.xpath("//*[@class='_dpmg2--controls-next']/i")).click();
		driver.findElement(By.xpath("//*[@class='_dpmg2--controls-next']/i")).click();
		driver.findElement(By.xpath("//*[@class='_dpmg2--controls-next']/i")).click();
		
		
		//Click en el primero de septiembre
		driver.findElement(By.xpath("/html/body/div[4]/div/div[4]/div[5]/div[4]/span[1]")).click();
		
		//click en fecha inicial
		driver.findElement(By.xpath("//input [@placeholder=\"Partida\" and @class=\"input-tag sbox-bind-disable-start-date sbox-bind-value-start-date-segment-1 sbox-bind-reference-flight-start-date-input -sbox-3-no-radius-right\"]" )).click();	
		//Click en Regreso
		driver.findElement(By.xpath("//input[@placeholder=\"Regreso\" and @class=\"input-tag -sbox-3-no-radius-left sbox-bind-disable-end-date sbox-bind-value-end-date sbox-bind-reference-flight-end-date-input\"]")).click();
		
		//driver.findElement(By.xpath("//input[@placeholder=\"Regreso\" and @class=\"input-tag -sbox-3-no-radius-left sbox-bind-disable-end-date sbox-bind-value-end-date sbox-bind-reference-flight-end-date-input\"]")).click();
		
		//Click en el 29 de septiembre
		
		driver.findElement(By.xpath("/html/body/div[4]/div/div[4]/div[5]/div[4]/span[29]")).click();
		
	}
	public void IngresarPasajeros()
	{
		driver.findElement(By.xpath("//input[@class=\"input-tag sbox-ui-no-focus-color sbox-bind-disable-passengers-input sbox-bind-reference-flight-roundtrip-passengers-input sbox-bind-reference-flight-plus-hotel-passengers-right-input\"]")).click();
		
		esperarSegundos(2);
		driver.findElement(By.xpath("/html/body/div[3]/div/div[1]/div[2]/div/div[1]/div/div[1]/div[2]/div/a[2]")).click();
									 
		
		
		//Click en aceptar
		driver.findElement(By.xpath("//*[@id=\"searchbox-sbox-all-boxes\"]/div[2]/div/div/div[3]/div[2]/div[4]/div/a/em")).click();

	}

	public static String CiudadesIguales() {
		return driver.findElement(By.xpath("//*[@id=\"searchbox-sbox-all-boxes\"]/div/div/div/div[3]/div[2]/div[1]/div[1]/div/div[2]/div/div/div/div/span[2]")).getText();
		
	}


}
