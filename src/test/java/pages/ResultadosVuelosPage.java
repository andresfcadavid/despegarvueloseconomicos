package pages;

import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import excel.EscribirExcel;

public class ResultadosVuelosPage {
	
	public void esperarSegundos(int segundos) {
		synchronized (driver) {
			try {
				driver.wait(segundos * 1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	

	EscribirExcel escribirexcel = new EscribirExcel();
	

		private  WebDriver driver;
		// Creo el constructor de �ste misma clase

		public ResultadosVuelosPage(WebDriver driver) {
			this.driver = driver;
		}

		public void ConsultarExcel() {
			String[] precio = new String[10];
			esperarSegundos(3);
			precio[0] = driver.findElement(By.xpath("//*[@id=\"clusters\"]/span[1]/span/cluster/div/div/span/fare/span/span/div[1]/item-fare/p/span/flights-price/span/flights-price-element/span/span/em/span[2]")).getText();
			precio[1] = driver.findElement(By.xpath("//*[@id=\"clusters\"]/span[2]/span/cluster/div/div/span/fare/span/span/div[1]/item-fare/p/span/flights-price/span/flights-price-element/span/span/em/span[2]")).getText();
			precio[2] = driver.findElement(By.xpath("//*[@id=\"clusters\"]/span[3]/span/cluster/div/div/span/fare/span/span/div[1]/item-fare/p/span/flights-price/span/flights-price-element/span/span/em/span[2]")).getText();
			precio[3] = driver.findElement(By.xpath("//*[@id=\"clusters\"]/span[4]/span/cluster/div/div/span/fare/span/span/div[1]/item-fare/p/span/flights-price/span/flights-price-element/span/span/em/span[2]")).getText();
			precio[4] = driver.findElement(By.xpath("//*[@id=\"clusters\"]/span[5]/span/cluster/div/div/span/fare/span/span/div[1]/item-fare/p/span/flights-price/span/flights-price-element/span/span/em/span[2]")).getText();
			precio[5] = driver.findElement(By.xpath("//*[@id=\"clusters\"]/span[6]/span/cluster/div/div/span/fare/span/span/div[1]/item-fare/p/span/flights-price/span/flights-price-element/span/span/em/span[2]")).getText();
			precio[6] = driver.findElement(By.xpath("//*[@id=\"clusters\"]/span[7]/span/cluster/div/div/span/fare/span/span/div[1]/item-fare/p/span/flights-price/span/flights-price-element/span/span/em/span[2]")).getText();
			precio[7] = driver.findElement(By.xpath("//*[@id=\"clusters\"]/span[8]/span/cluster/div/div/span/fare/span/span/div[1]/item-fare/p/span/flights-price/span/flights-price-element/span/span/em/span[2]")).getText();
			precio[8] = driver.findElement(By.xpath("//*[@id=\"clusters\"]/span[9]/span/cluster/div/div/span/fare/span/span/div[1]/item-fare/p/span/flights-price/span/flights-price-element/span/span/em/span[2]")).getText();
			precio[9] = driver.findElement(By.xpath("//*[@id=\"clusters\"]/span[10]/span/cluster/div/div/span/fare/span/span/div[1]/item-fare/p/span/flights-price/span/flights-price-element/span/span/em/span[2]")).getText();
			
		       try {
                   EscribirExcel.writeExcel(precio);
            } catch (IOException e) {
                   // TODO Auto-generated catch block
                   e.printStackTrace();
            }
            

			
		}

		public  String listadoVuelos() {
			
			return driver.getTitle();
		}
		
	}
	

