package steps_definition;

import static org.junit.Assert.assertEquals;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import pages.DespegarHomePage;
import pages.ResultadosVuelosPage;

//Cambiando escenarios a m�todos
public class BuscarVuelosSteps {
	// Precondiciones para el comienzo de la ejecuci�n
	private WebDriver driver;
	private DespegarHomePage despegarHomePage;
	private ResultadosVuelosPage ResultadosVuelosPage;

	@Before
	public void setUp() {

		String exePath = "C:\\driver\\chromedriver.exe"; // ruta del driver
		System.setProperty("webdriver.chrome.driver", exePath);// propiedad que indica la posici�n del driver
		// Instancio para cada met;odo
		ChromeOptions options = new ChromeOptions();
		options.addArguments("--start-maximized");
		driver = new ChromeDriver(options);
		driver.get("https://www.despegar.com.co/");
		
		
		despegarHomePage = new DespegarHomePage(driver);
		ResultadosVuelosPage = new ResultadosVuelosPage(driver);
		

	}

	@Given("^Que el usuario esta en la pagina Despegar$")
	public void que_el_usuario_esta_en_la_pagina_Despegar() {
		despegarHomePage.cerrarAprovechaNuestrosBeneficios();
		despegarHomePage.clicEnVuelos();
	}

	@When("^Ingresa ciudad origen \"(.*)\"$")
	public void ingresa_la_informacion_de_busqueda_de_origen(String origen) {
		despegarHomePage.IngresarOrigen(origen);
	}
	
	
	@And("^Ingresa Ciudad destino \"(.*)\"$")
	public void ingresa_la_informacion_de_busqueda_de_destino(String destino) {
		despegarHomePage.IngresarDestino(destino);
	}
	
	

	@When("^Busca los vuelos$")
	public void busca_los_vuelos() {
		despegarHomePage.IngresarFecha();
		despegarHomePage.IngresarPasajeros();

	}
	
	@When("^Listar los vuelos mas economicos en excel$")
	public void listarvuelos() {
		ResultadosVuelosPage.ConsultarExcel();
		
		
	}

	@Then("^Se muestran los vuelos disponibles$")
	public void se_muestran_los_vuelos_disponibles() {
		String result = ResultadosVuelosPage.listadoVuelos();
		assertEquals("Despegar.com . Resultados de Vuelos", result);
		
		
	}
	
	@Then("^Mensaje de error ciudad origen destino iguales$")
	public void CiudadesIgualesError() {
		String result = DespegarHomePage.CiudadesIguales();
		assertEquals("El destino debe ser diferente del origen", result);
}
}
