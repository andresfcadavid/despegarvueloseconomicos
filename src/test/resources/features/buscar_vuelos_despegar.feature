@tag
Feature: Buscar vuelos economicos  
 yo como viajero 
 Quiero buscar vuelos economicos entre las ciudades de medellin y cartagena
 Para minimizar mis costos de viaje

  @CasoExitoso
  Scenario: Realizar busqueda de vuelo exitosa
    Given Que el usuario esta en la pagina Despegar
    When Ingresa ciudad origen "Medellin, Colombia"
    And Ingresa Ciudad destino "Cartagena de Indias, Colombia"
    And Busca los vuelos
    And Listar los vuelos mas economicos en excel
    Then Se muestran los vuelos disponibles
    
    
   
    @CasoFallidoOrigenDestinoIgual
  Scenario: Busqueda fallida ciudades iguales en las busqueda
    Given Que el usuario esta en la pagina Despegar
    When Ingresa ciudad origen "Cartagena de Indias, Colombia"
    And Ingresa Ciudad destino "Cartagena de Indias, Colombia"
    And Busca los vuelos
    Then Mensaje de error ciudad origen destino iguales
    
    
    @CasoFallidoDestinoVacio
  Scenario: Busqueda no exitosa destino vacio
    Given Que el usuario esta en la pagina Despegar
    When Ingresa ciudad origen "Medellin, Colombia"
    And Ingresa Ciudad destino ""
    And Busca los vuelos
    And Listar los vuelos mas economicos en excel
    Then Mensaje de error ingresa un destino
    