+#Reto 2 Automatizaci�n
+#Descripci�n: 
+De acuerdo a la especificaci�n entregada se requiere  realizar b�squedas de vuelos en la p�gina Despegar.com entre dos ++ciudades y fechas indicadas, acto seguido seleccionar llevar a un archivo de Excel los diez vuelos m�s econ�micos y marcar ++con color verde el menos costoso de ellos. 
Se realiza escenario de ejecuci�n exitosa, as� como escenarios negativos 


+#Framework de automatizacion: Cucumber
+#Compilador: JAVA
+#Patrones de desarrollo: Se utiliza BDD (Desarrollo guiado por comportamientos)
+#Pr�cticas de automatizaci�n:
+Se crea la siguiente estructura dentro del proyecto
/despegar/src/test/java: Clases requeridas para la ejecuci�n
/despegar/src/test/resources:  modelado en BDD
/despegar/src : Se encuentra el POM
+#Herramienta de automatizacion:Selenium
+#Estrategia de automatizaci�n:
+Se modela en cucumber los escenarios (BDD) de prueba que van a ser utilizados dentro de la automatizaci�n
+Se tiene una clase donde se definen los steps de la prueba "BuscarVuelosSteps", requeridos desde la historia de usuario 
+Se crean dos clases DespegarHomePage y ResultadosVuelosPage, en las cuales se automatizan los fujos requeridos en la ++pagina despegar.com. En la selecci�n de vuelos y resultado de la busqueda de dichos vuelos   
+Se construye una clase en java "EscribirExcel" encargada  de escribir en un documento de excel el resultado de la busqueda 
realizada.
+Se crea la clase LoginRunner donde agregan los tags de los escenarios que se van a ejecutar (JUnit)


+#Navegador:Chrome Versi�n 66.0.3359.181 (Build oficial) (64 bits)
+#Conclusiones de la automratizacion: Se logra cumplir con el objetivo del reto, adicionalmente de manera personal me adentro ++en conocimientos en JAVA y herramientas de automatizaci�n 
 